# 使用ROS操控APM(Ardupilot)

> [Ardupilot docs: ROS](https://ardupilot.org/dev/docs/ros.html)

环境：

PC端

| 项目             | 版本                         |
| ---------------- | ---------------------------- |
| 系统架构         | x86                          |
| 操作系统运行环境 | WSL2（Windows Sub System 2） |
| 操作系统         | Ubuntu 18.04                 |
| ROS              | Melodic                      |

机载电脑端

| 项目             | 版本         |
| ---------------- | ------------ |
| 操作系统运行环境 | 直接运行     |
| 操作系统         | Ubuntu 18.04 |
| ROS              | Melodic      |

## ROS安装

> [ROS官方安装教程](http://wiki.ros.org/cn/ROS/Installation)

注意安装的版本要对应Ubuntu版本

### 查看ubuntu版本

```shell
cat /etc/issue
```

我的Ubuntu版本是18.04，所以安装推荐的ROS Melodic版本

### 配置软件仓库

> [How to enable/disable Universe, Multiverse and Restricted repository on Ubuntu 20.04 LTS Focal Fossa - Linux Tutorials - Learn Linux Configuration](https://linuxconfig.org/how-to-enable-disable-universe-multiverse-and-restricted-repository-on-ubuntu-20-04-lts-focal-fossa)

配置你的Ubuntu软件仓库（repositories）以允许使用“restricted”“universe”和“multiverse”存储库。

这步操作可以使用Ubuntu的GUI进行，也可以使用命令行进行，由于我希望安装在WSL2中，所以使用命令行。

1. 使用`add-apt-repository`命令

    ```shell
    sudo add-apt-repository universe
    sudo add-apt-repository multiverse
    sudo add-apt-repository restricted
    ```

2. 检查启用的软件仓库

    ```shell
    grep ^deb /etc/apt/sources.list
    ```

    结果可能如下

    ```shell
    deb https://mirrors.tuna.tsinghua.edu.cn/ubuntu/ jammy main restricted universe multiverse
    deb https://mirrors.tuna.tsinghua.edu.cn/ubuntu/ jammy-updates main restricted universe multiverse
    deb https://mirrors.tuna.tsinghua.edu.cn/ubuntu/ jammy-backports main restricted universe multiverse
    deb http://security.ubuntu.com/ubuntu/ jammy-security main restricted universe multiverse
    ```

    代表软件仓库已经启用

3. 如果想要移除软件仓库（可选）

    ```shell
    sudo add-apt-repository --remove universe
    sudo add-apt-repository --remove multiverse
    sudo add-apt-repository --remove restricted
    ```

### 设置source.list

设置电脑以安装来自packages.ros.org的软件（使用清华源）

```shell
sudo sh -c '. /etc/lsb-release && echo "deb http://mirrors.tuna.tsinghua.edu.cn/ros/ubuntu/ `lsb_release -cs` main" > /etc/apt/sources.list.d/ros-latest.list'
```

### 设置密钥

```shell
sudo apt-key adv --keyserver 'hkp://keyserver.ubuntu.com:80' --recv-key C1CF6E31E6BADE8868B172B4F42ED6FBAB17C654
```

### 开始安装

更新软件源

```
sudo apt update
```

如果是在PC上安装，选择**桌面完整版**

```shell
sudo apt install ros-melodic-desktop-full
```

如果只是在机载电脑上安装，选择**基础包**

```shell
sudo apt install ros-melodic-ros-base
```

### rosdep

> [[rosdep update更新超时问题解决\]一种新方法,亲测有用,2022.3.12_Kasout的博客-CSDN博客](https://blog.csdn.net/qq_46387453/article/details/123446413)

### 设置环境

启动bash自动添加环境

```shell
echo "source /opt/ros/melodic/setup.bash" >> ~/.bashrc
source ~/.bashrc
```

### 安装MAVROS

```shell
sudo apt-get install ros-melodic-mavros ros-melodic-mavros-extras
wget https://raw.fastgit.org/mavlink/mavros/master/mavros/scripts/install_geographiclib_datasets.sh --no-check-certificate
./install_geographiclib_datasets.sh
```

### 安装RQT

```shell
sudo apt-get install ros-melodic-rqt ros-melodic-rqt-common-plugins ros-melodic-rqt-robot-plugins
```

## 机载电脑连接APM飞控

> [Connecting with ROS — Dev documentation (ardupilot.org)](https://ardupilot.org/dev/docs/ros-connecting.html)
>
> [mavros - ROS Wiki](http://wiki.ros.org/mavros#Usage)
>
> [树莓派+PX4固件+T265+MAVROS+QGC实现室内定位_通过ros2实现树莓派和px4通讯_frühes morgen的博客-CSDN博客](https://blog.csdn.net/weixin_45613903/article/details/108262467)

### 接线

> [Pixhawk 飞控接口线序图 & 常用传感器连接设置(QGC) - 知乎 (zhihu.com)](https://zhuanlan.zhihu.com/p/611061554)

使用的飞控硬件是Pixhawk2.4.8

接口是SH1.25

![img](_media/v2-80ce8a397570e63c75fb672fb951d697_r.jpg)

### WSL2连接串口

WSL2不能直接使用USB设备，需要使用开源项目USBIPD-WIN

> 按以下教程安装：
>
> [连接 USB 设备 | Microsoft Learn](https://learn.microsoft.com/zh-cn/windows/wsl/connect-usb)

附加USB设备

以管理员模式打开Powershell

```powershell
usbipd wsl list
```

选择要附加到WSL的设备总线ID

```powershell
usbipd wsl attach --busid <busid>
```

打开Ubuntu，列出附加的usb设备

```shell
lsusb
```

如果想要断开附加设备，可以物理断开，也可以以管理员模式启动Powershell

```powershell
usbipd wsl detach --busid <busid>
```

断开后仍需运行附加命令才能在WSL2中使用

### 配置飞控参数

1. 使用地面站软件连接飞控
2. 打开参数树，根据对应物理接口找到对应参数位置，TELEM1对SERIAL1，TELEM2对应SERIAL2
3. SERIALx_BAUD调节波特率，比如57对应57600，SERIALx_PROTOCOL选2对应MAVLink2

### 运行MAVROS节点

运行前先查看串口的设备号

```shell
ls /dev/tty*
```

设备号可能是/dev/ttyUSB0

修改apm.launch的默认fcu_url，方便之后使用

```shell
roscd mavros
sudo vim launch/apm.launch
```

```xml
<!-- 修改下行为，ttyUSB0修改为查到的设备号 -->
<arg name="fcu_url" default="/dev/ttyUSB0:57600" />
```

赋予串口权限

```shell
sudo chmod 777 /dev/ttyUSB0
```

现在如果启动MAVROS节点，会警告时间同步速率太高

```
TM : RTT too high for timesync: 16.71 ms.
```

修改apm_config.yaml，修改为

```yaml
conn:
	...
	timesync_rate: 0.0 # feature disabled
	...
```

运行mavros node

```shell
roslaunch mavros apm.launch
```

查看mavros运行状态

```shell
rostopic echo /mavros/state
```

如果`connected: True`代表连接成功

测试控制飞控指令

**切换飞控模式**

```shell
rosrun mavros mavsys mode -c 0 # sets the vehicle to mode "0"
```

> [mavros/CustomModes - ROS Wiki](http://wiki.ros.org/mavros/CustomModes)
>
> 四旋翼无人机查看APM:Copter栏目
>
> 常用模式的编码
>
> | 编号 | 模式      |
> | ---- | --------- |
> | 0    | STABILIZE |
> | 5    | LOITER    |
> | 8    | POSITION  |

查看mavros运行模式是否切换

```shell
rosrun mavros mavsys mode -c 5
rostopic echo /mavros/state
```

如果`mode: "LOITER"`，代表模式切换成功。

**解锁**

```shell
rosrun mavros mavsafety arm
```

**锁定**

```shell
rosrun mavros mavsafety disarm
```

## ROS控制APM起飞降落

> [解决Ardupilot+gazebo+mavros在仿真状态下无人机能解锁，但是不能起飞的问题_ardupilot仿真飞机不动_一点儿也不萌的萌萌的博客-CSDN博客](https://blog.csdn.net/u011341856/article/details/119533774)
>
> [MAVROS Offboard 控制示例 (C++) | PX4 自动驾驶用户指南](https://docs.px4.io/main/zh/ros/mavros_offboard_cpp.html)

```c++
/* 起飞，保持，降落 */

#include <ros/ros.h>
#include <geometry_msgs/PoseStamped.h>
#include <mavros_msgs/CommandBool.h>
#include <mavros_msgs/CommandTOL.h>
#include <mavros_msgs/SetMode.h>
#include <mavros_msgs/State.h>

static mavros_msgs::State current_state; // 无人机当前状态
static mavros_msgs::SetMode set_mode; // 设置的模式
static mavros_msgs::CommandBool arming; // 解锁锁定
static mavros_msgs::CommandTOL takeoff_land; // 起飞降落
static geometry_msgs::PoseStamped pose; // 位姿
// 无人机状态回调函数
void state_callback(const mavros_msgs::State::ConstPtr &msg){
    current_state = *msg;
}

int main(int argc, char **argv){
    ros::init(argc, argv, "takeoff_land");
    ros::NodeHandle n;

    // 订阅无人机状态
    ros::Subscriber state_sub = n.subscribe("/mavros/state", 10, state_callback);
    // 设置模式服务
    ros::ServiceClient set_mode_cli = n.serviceClient<mavros_msgs::SetMode>("/mavros/set_mode");
    // 解锁服务
    ros::ServiceClient arming_cli = n.serviceClient<mavros_msgs::CommandBool>("/mavros/cmd/arming");
    // 起飞服务
    ros::ServiceClient takeoff_cli = n.serviceClient<mavros_msgs::CommandTOL>("/mavros/cmd/takeoff");
    // 降落服务
    ros::ServiceClient land_cli = n.serviceClient<mavros_msgs::CommandTOL>("/mavros/cmd/land");
    // 发布设定位姿
    ros::Publisher local_pos_pub = n.advertise<geometry_msgs::PoseStamped>("/mavros/setpoint_position/local", 10);
    // 定义循环频率
    ros::Rate loop_rate(20); // 单位：Hz

    // 等待连接飞控
    ROS_INFO("Waiting for connection...");
    while(ros::ok() && !current_state.connected){
        ros::spinOnce();
        loop_rate.sleep();
    }
    ROS_INFO("Connected.");

    // 设置无人机模式为Stabilize
    set_mode.request.base_mode = 0; // 0 if custom_mode != ''
    // 可设置的模式：http://wiki.ros.org/mavros/CustomModes
    set_mode.request.custom_mode = "STABILIZE";
    if(set_mode_cli.call(set_mode)){
        ROS_INFO("Set mode with value %d", set_mode.response.mode_sent);
    }else{
        ROS_ERROR("Fail to set mode.");
        return -1;
    }
    
    // 解锁无人机
    arming.request.value = true;
    if(arming_cli.call(arming)){
        ROS_INFO("Arm with value %d", arming.response.success);
    }else{
        ROS_ERROR("Fail to arm.");
    }

    // 设置无人机模式为Guided
    set_mode.request.base_mode = 0; // 0 if custom_mode != ''
    // 可设置的模式：http://wiki.ros.org/mavros/CustomModes
    set_mode.request.custom_mode = "GUIDED";
    if(set_mode_cli.call(set_mode)){
        ROS_INFO("Set mode with value %d", set_mode.response.mode_sent);
    }else{
        ROS_ERROR("Fail to set mode.");
        return -1;
    }

    // 保持
    ros::Duration(3).sleep();

    // 发送起飞指令
    takeoff_land.request.altitude = 0.5;
    takeoff_land.request.latitude = 0;
    takeoff_land.request.longitude = 0;
    takeoff_land.request.min_pitch = 0;
    takeoff_land.request.yaw = 0;
    if(takeoff_cli.call(takeoff_land)){
        ROS_INFO("Takeoff with value %d", takeoff_land.response.success);
    }else{
        ROS_ERROR("Fail to takeoff.");
    }

    // 保持
    ros::Duration(10).sleep();

    // 发送降落指令
    takeoff_land.request.altitude = 0;
    takeoff_land.request.latitude = 0;
    takeoff_land.request.longitude = 0;
    takeoff_land.request.min_pitch = 0;
    takeoff_land.request.yaw = 0;
    if(land_cli.call(takeoff_land)){
        ROS_INFO("Land with value %d", takeoff_land.response.success);
    }else{
        ROS_ERROR("Fail to land.");
    }

    return 0;
}

```

**注意：**

- LOITER是悬停模式，可以通过遥控器控制位置、方向，但是不能通过set_point话题来控制位姿
- 想要控制位姿，使用GUIDED模式，这个模式类似于PX4中的offboard模式，不过APM只用发布一次目标位置，PX4需要一直发布，否则会退出offboard模式
- 切换GUIDED和LOITER模式需要获取GPS Lock，这里采用先用STABILIZE模式解锁，再切换到GUIDED模式

### GUIDED模式介绍

> [GUIDED Mode — Plane documentation (ardupilot.org)](https://ardupilot.org/plane/docs/guided-mode.html)

根据官方文档：

GUIDED模式适用于不设置mission的情况下让飞机飞到指定点，多数地面站软件上有“飞到这里”的功能，使用该功能就会进入GUIDED模式，飞到指定点，然后进入LOITER模式悬停。

GUIDED模式的另一个应用是地理围栏，当飞机飞越地理围栏时，就会进入GUIDED模式，然后自动飞向预先指定的地理围栏返回点然后悬停，直到飞手重新接管飞机。

## ROS控制APM前进

### 存在问题

- 出现无法获取无人机local_position、imu等数据的情况，给飞控重新上电无法解决，但是重连地面站解决
