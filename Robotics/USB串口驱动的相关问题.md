# USB串口驱动的相关问题

> [WCHSoftGroup/ch343ser_linux: USB driver for USB to serial chip ch342, ch343, ch344, ch9101, ch9102, ch9103, etc (github.com)](https://github.com/WCHSoftGroup/ch343ser_linux)

## Linux下的USB串口驱动

沁恒微电子的ch342/ch343/ch344/ch347/ch9101/ch9102/ch9103系列芯片**完全兼容**USB的Communications Device Class (CDC)标准，可以和标准的CDC-ACM(Abstract Control Model)协同工作。

Linux操作系统提供一个默认的CDC-ACM驱动，驱动名为cdc-acm。但是这个通用驱动对于控制特定设备的**能力有限**，它不知道特定设备的协议。所以设备产商可以创建定制驱动来支持一些特定功能，比如**硬件流控**或GPIO。

### Linux识别出CH343P，但是串口无数据，且打开缓慢

根据沁恒github说法，Linux自带的CDC-ACM驱动无法使用串口设备的硬件流控，所以可以用官方提供的`CH34xSerCfg`工具，取消`CDC模式下启用(CTS/RTS)硬件流控`，就可以正常使用了

![image-20230714213211498](_media/image-20230714213211498.png)