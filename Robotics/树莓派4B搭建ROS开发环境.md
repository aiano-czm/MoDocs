# 树莓派4B搭建ROS开发环境

## 树莓派系统安装

### 下载镜像

> https://www.raspberrypi.org/downloads/
>
> [Index of /ubuntu-cdimage/ubuntu/releases/18.04/release/ | 清华大学开源软件镜像站 | Tsinghua Open Source Mirror](https://mirrors.tuna.tsinghua.edu.cn/ubuntu-cdimage/ubuntu/releases/18.04/release/)

## 3.5寸触摸屏驱动

> [3.5inch RPi Display - LCD wiki](http://www.lcdwiki.com/zh/3.5inch_RPi_Display)

安装驱动

```shell
sudo rm -rf LCD-show
git clone https://github.com/goodtft/LCD-show.git
chmod -R 755 LCD-show
cd LCD-show/
sudo ./LCD35-show 
```

旋转显示方向

```shell
cd LCD-show/
sudo ./rotate.sh 90 
```

## SSH连接

查看树莓派ip地址

