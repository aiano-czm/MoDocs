# JetsonNano使用GPIO并发布ROS服务

> [cn/ROS/Tutorials/WritingServiceClient(python) - ROS Wiki](http://wiki.ros.org/cn/ROS/Tutorials/WritingServiceClient(python))
>
> [msg - ROS Wiki](http://wiki.ros.org/msg)
>
> [rospy/Overview/Logging - ROS Wiki](http://wiki.ros.org/rospy/Overview/Logging)
>
> [Jetson Nano 入坑之路 ---- （6）GPIO使用（输入输出与回收）_星羽空间的博客-CSDN博客](https://blog.csdn.net/qq_25662827/article/details/119380347)
>
> [NVIDIA/jetson-gpio: A Python library that enables the use of Jetson's GPIOs (github.com)](https://github.com/NVIDIA/jetson-gpio)
>
> [rospy/Overview/Messages - ROS Wiki](http://wiki.ros.org/rospy/Overview/Messages)
