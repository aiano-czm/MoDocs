# 使用ROS操控PX4

## PX4相关配置

### 微空光流配置

> [MTF模块 PX4 光流模块详细配置_TLKids的博客-CSDN博客](https://blog.csdn.net/TLKids/article/details/130200030?spm=1001.2014.3001.5502)

### 连接地面站可以解锁，不连地面站不能解锁

`参数`->`Developer`->`Circuit Breaker`->`CBRK_SUPPLY_CHK`设置为894281，禁用电源有效检查

原因不明

### 解锁后电机怠速

> [成功解决Pixhawk怠速（一解锁电机就转）_pix解锁怠速参数_大强强小强强的博客-CSDN博客](https://blog.csdn.net/weixin_41869763/article/details/103756641)

怠速指的是解锁后电机就开始旋转

修改`PWM_MIN`参数为0可以解决，但是建议不要修改，因为会改变油门行程

## MAVROS控制PX4

> [Offboard控制 | PX4 自动驾驶用户指南](https://docs.px4.io/main/zh/ros/offboard_control.html)
>
> [mavros - ROS Wiki](https://wiki.ros.org/mavros)
>
> [mavros/CustomModes - ROS Wiki](https://wiki.ros.org/mavros/CustomModes)
>
> [MAVROS Offboard 控制示例 (C++) | PX4 自动驾驶用户指南](https://docs.px4.io/main/zh/ros/mavros_offboard_cpp.html)

切换飞控模式

```shell
rosrun mavros mavsys mode -c 0 # sets the vehicle to mode "0"
```

### PX4和MAVROS坐标系的区别

> [MAVROS Offboard 控制示例 (C++) | PX4 自动驾驶用户指南](https://docs.px4.io/main/zh/ros/mavros_offboard_cpp.html)
>
> [mavros坐标系转换与方向_coordinate_frame_集智飞行的博客-CSDN博客](https://blog.csdn.net/benchuspx/article/details/112970682)
>
> 尽管PX4在航空航天常用的NED坐标系下操控飞机，但MAVROS将自动将该坐标系切换至常规的ENU坐标系下，反之亦然。 这也就是为什么我们设置`z`为+2。

PX4采用**NED（北东地）**坐标系或**FRD（前右下）**坐标系

mavros采用**ENU（东北天）**坐标系或body系

### Ubuntu串口改名

> [Ubuntu下绑定串口方法一_mrpanwei的博客-CSDN博客](https://blog.csdn.net/s261676224/article/details/117962881)