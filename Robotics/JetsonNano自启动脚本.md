# JetsonNano自启动脚本

> [linux的/etc/rc.local文件(开机自启)_黑莹de希望的博客-CSDN博客](https://blog.csdn.net/ws_kfxd/article/details/110088503)
>
> [robot_upstart - ROS Wiki](http://wiki.ros.org/robot_upstart)
>
> [ROS设置开机自启launch文件_StephenJSL的博客-CSDN博客](https://blog.csdn.net/jiangsala/article/details/88796261)
>
> [ubuntu开机脚本自启动(含ROS程序启动)方法_开机自启动需要运行ros1的程序吗_北方南方2020的博客-CSDN博客](https://blog.csdn.net/baidu_34319491/article/details/106456571)
>
> [使用robot-upstart开机自启动ROS程序_robot_upstart_金禾的博客-CSDN博客](https://blog.csdn.net/zxc1209642212/article/details/108241020)

## 一些指令

### 指令后台运行

```shell
<指令> &
```

后台运行时，程序的输出仍然可以打印到终端，无法通过Ctrl-C关闭程序，可以通过关闭终端关闭程序

### 运行的程序转到后台

```shell
bg
```

此时会显示后台程序的PID和任务编号

### 后台程序转到前台

```shell
fg
```

```shell
fg %n # 将编号为n的任务转到前台运行
```

### 查看当前终端的后台任务

```shell
jobs
```

### 查看所有终端的后台任务

```shell
ps -aux | grep "你需要关闭的进程名字" #a:显示所有程序 u:以用户为主的格式来显示 x:显示所有程序，不以终端机来区分
```

### 关闭进程

```shell
kill <进程号>
```

### 查看当前终端名

```shell
tty
```

### 显示开启的虚拟终端

```shell
ls /dev/pts/*
```



## 使用robot-upstart开机启动ROS

> [robot_upstart - ROS Wiki](http://wiki.ros.org/robot_upstart)
>
> [Upstart for ROS Robots — robot_upstart 0.2.2 documentation](http://docs.ros.org/en/jade/api/robot_upstart/html/)
>
> [使用robot-upstart开机自启动ROS程序_robot_upstart_金禾的博客-CSDN博客](https://blog.csdn.net/zxc1209642212/article/details/108241020)

### 安装

```shell
sudo apt install ros-<distro>-robot-upstart
```

### 添加自启动任务

```shell
rosrun robot_upstart install --setup ~/<ros_workspace>/devel/setup.bash <my_pkg>/launch/<launch file> --logdir ~/logs
```

会显示添加的自启动任务名

```shell
sudo systemctl daemon-reload && sudo systemctl start <自启动任务名>
```

重启

### 关闭程序

```shell
ps -aux | grep <自启动任务名> # 查看PID
sudo kill <进程ID>
```

### 移除自启动任务

```shell
rosrun robot_upstart uninstall <自启动任务名>
```

