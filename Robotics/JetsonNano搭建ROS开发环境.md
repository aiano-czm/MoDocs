# Jetson Nano 搭建ROS开发环境

> [Jetson Nano 官方网站](https://www.nvidia.cn/autonomous-machines/embedded-systems/jetson-nano/)

## 远程连接

### SSH

1. PC和Jetson连接同一个WiFi

2. Jetson中打开Terminal，输入：

    ```shell
    ifconfig
    ```

3. 找到`wlan0`，其中的`inet 192.168.3.80`（诸如此类）即为Jetson的局域网IP地址

4. 使用PuTTY、Xshell、MobaXterm、VSCode或是终端ssh等工具连接查到的IP，参考命令：

    ```shell
    ssh username@ip_address
    ```

5. 第一次连接输入yes，等待一会即可连接成功。

### VNC

> 参考：[Jetson nano VNC远程桌面配置 - Michael云擎 - 博客园 (cnblogs.com)](https://www.cnblogs.com/cloudrivers/p/12110117.html)

安装Vino：

```shell
sudo apt install vino
```

编辑配置：

```shell
sudo vi /usr/share/glib-2.0/schemas/org.gnome.Vino.gschema.xml
```

在最后添加：

```shell
<key name="enabled" type="b">
	<summary>Enable remote access to the desktop</summary>
	<description>
		If true, allows remote access to the desktop via the RFB
		protocol. Users on remote machines may then connect to the
		desktop using a VNC viewer.
	</description>
	<default>false</default>
</key>
```

编译：

```shell
sudo glib-compile-schemas /usr/share/glib-2.0/schemas
```

启动Vino-Server：

```shell
# 直接在Jetson里启动
/usr/lib/vino/vino-server
# 通过SSH启动
/usr/lib/vino/vino-server --display=:0
```



## 安装ROS

查看磁盘剩余空间

```shell
df -hl
```

参考安装指南

> [ROS 安装指南](http://wiki.ros.org/cn/ROS/Installation)

rosdep国内修复

> [rosdep](https://blog.csdn.net/qq_46387453/article/details/123446413)