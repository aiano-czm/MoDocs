# USB一键下载

## 原理

USB转UART芯片上通常带有许多流控引脚，至少包含DTR和RTS两个引脚，这两个引脚的电平可以被PC上位机控制，通过在后面添加三极管逻辑控制电路，可以控制STM32进行复位和进入Bootloader，从而实现一键自动下载固件。

想要使用串口给STM32烧录程序，需要进入DFU（Development Firmware Upgrade）模式，进入的方法是BOOT0拉高，BOOT1拉低，NRST拉低复位。在许多简单的核心板中，这个步骤是通过跳线帽或者按键手动进行的。

## 电路

> [免外围电路的单片机串口一键下载方案](https://www.wch.cn/application/575.html)

**STM32Fxxx/CH32Fxxx/CH32Vxxx等系列MCU** 

![img](_media/6rGAjBbclrc6aBCFG2kPU72ix9d3Q5Ru3WdUvelS.jpeg)

MCU启动模式：

![img](_media/G99jAWk76WTpilY6CNy9Cn6b9rjvVeBKXM4XMzrr.jpeg)

MCU为低电平复位，硬件上需要满足MCU复位后自动进入“串口下载”模式。下载时序示意图：

![img](_media/N4HrNbEh1ktlp7pfYspZseo45mzSsEPRE5d03rjD.jpeg)

## 上位机软件

> [FlyMcu](http://www.mcuisp.com/chinese%20mcuisp%20web/ruanjianxiazai-chinese.htm)