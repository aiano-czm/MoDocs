# K210本地训练环境搭建

## ubuntu

重新安装一个ubuntu 20.04，专门用于训练，避免其它影响

## anaconda

> [Ubuntu 安装 conda - 知乎 (zhihu.com)](https://zhuanlan.zhihu.com/p/459607806)

安装anaconda

```shell
wget https://mirrors.bfsu.edu.cn/anaconda/archive/Anaconda3-2023.07-1-Linux-x86_64.sh --no-check-certificate
```

> [Index of / (anaconda.com)](https://repo.anaconda.com/archive/?C=M&O=A)

```shell
bash Anaconda3-2023.07-1-Linux-x86_64.sh
```

取消conda打开终端自动启动

```shell
conda config --set auto_activate_base false
```

conda换源

> [Ubuntu anaconda换源(最新最全亲测)_向日葵骑士Faraday的博客-CSDN博客](https://blog.csdn.net/KIK9973/article/details/118776314)
>
> [anaconda | 镜像站使用帮助 | 清华大学开源软件镜像站 | Tsinghua Open Source Mirror](https://mirrors.tuna.tsinghua.edu.cn/help/anaconda/)

```shell
vim .condarc

# 粘贴如下内容
channels:
  - defaults
show_channel_urls: true
default_channels:
  - https://mirrors.tuna.tsinghua.edu.cn/anaconda/pkgs/main
  - https://mirrors.tuna.tsinghua.edu.cn/anaconda/pkgs/r
  - https://mirrors.tuna.tsinghua.edu.cn/anaconda/pkgs/msys2
custom_channels:
  conda-forge: https://mirrors.tuna.tsinghua.edu.cn/anaconda/cloud
  msys2: https://mirrors.tuna.tsinghua.edu.cn/anaconda/cloud
  bioconda: https://mirrors.tuna.tsinghua.edu.cn/anaconda/cloud
  menpo: https://mirrors.tuna.tsinghua.edu.cn/anaconda/cloud
  pytorch: https://mirrors.tuna.tsinghua.edu.cn/anaconda/cloud
  pytorch-lts: https://mirrors.tuna.tsinghua.edu.cn/anaconda/cloud
  simpleitk: https://mirrors.tuna.tsinghua.edu.cn/anaconda/cloud
  deepmodeling: https://mirrors.tuna.tsinghua.edu.cn/anaconda/cloud/

# 清除缓存索引，保证用的是新镜像站索引
conda clean -i
# 查看是否换源成功（channel URLs）
conda info
```

conda升级

```shell
conda update -n base -c defaults conda
```

创建虚拟环境

> [tensorflow支持的python版本](https://tensorflow.google.cn/install/source_windows?hl=zh-cn#cpu)
>
> tensorflow2.3.1支持python3.5-3.8

```shell
conda create -n k210_train python=3.8
```

删除虚拟环境

```
conda remove -n k210_train --all
```

查看所有虚拟环境

```shell
conda info -e
```

进入虚拟环境

```shell
conda activate k210_train
```

退出虚拟环境

```shell
conda deactivate
```

## 训练环境搭建

克隆代码（需要先配置github SSH）

```shell
git clone git@github.com:sipeed/maix_train.git --recursive
```

安装依赖（确保已经进入虚拟环境）

```shell
cd maix_train
pip3 install -r requirements.txt -i https://mirrors.aliyun.com/pypi/simple/
```

降级protobuf

> [TypeError: Descriptors cannot not be created directly解决_强殖装甲凯普的博客-CSDN博客](https://blog.csdn.net/qq_38163755/article/details/125071007)

```shell
pip install 'protobuf~=3.19.0'
```

下载[nncase v0.1.0-rc5](https://github.com/kendryte/nncase/releases/tag/v0.1.0-rc5)，选择`ncc-linux-x86_64.tar.xz`

创建文件夹`maix_train/tools/ncc/ncc_v0.1`解压到该文件夹内

确保ncc可执行文件的路径为`maix_train/tools/ncc/ncc_v0.1/ncc`

初始化工程

```shell
python3 train.py init
```

编辑配置

```shell
vim instance/config.py
```

## 本地训练

### 分类模型

数据集为压缩包

```shell
python3 train.py -t classifier -z datasets/test_classifier_datasets.zip train
```

数据集为文件夹

```shell
python3 train.py -t classifier -d datasets/test_classifier_datasets train
```

### 目标检测模型

数据集为压缩包

```shell
python3 train.py -t detector -z datasets/test_detector_xml_format.zip train
```

数据集为文件夹

```shell
python3 train.py -t detector -d datasets/test_detector_xml_format train
```

### 训练结果

训练结果保存在`out`文件夹，会生成一个`zip`文件，把它解压到SD卡根目录

修改`boot.py`

```python
...
labels = [...] # 这一行用labels.txt文件替换
...

...
# 根据模型是写入flash还是保存在SD卡选择
# main(anchors = anchors, labels=labels, model_addr=0x300000, lcd_rotation=2, sensor_window=(240, 240))
main(anchors = anchors, labels=labels, model_addr="/sd/m.kmodel")
...
```

上电即可运行

## 数据集标注

> [heartexlabs/labelImg: LabelImg is now part of the Label Studio community. The popular image annotation tool created by Tzutalin is no longer actively being developed, but you can check out Label Studio, the open source data labeling tool for images, text, hypertext, audio, video and time-series data. (github.com)](https://github.com/heartexlabs/labelImg)
>
> [最简单Labelimg安装教程，下载直接运行.exe文件即可_labelimg exe_weixin_45086338的博客-CSDN博客](https://blog.csdn.net/weixin_45086338/article/details/115611488)

下载标注工具`labelimg.exe`

- Open Dir 打开图片文件夹
- Change Save Dir 修改XML文件夹
- `A`和`D`切换前后图片
- `W`画框，输入标签
- `Ctrl+S`保存标注，保存的XML文件名与图片同名

最后保证数据集结构如下

```shell
<数据集名>/
├── images
│   ├── <label1>
│   └── <label2>
├── labels.txt
└── xml
    ├── <label1>
    └── <label2>
```

<label1>和<label2>各自放图片和XML文件

`labels.txt`内容为

```
label1
label2
```

